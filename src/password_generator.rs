use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};

pub fn generate_password(l: usize) -> Option<String> {
    let pass_out: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(l)
        .map(char::from)
        .collect();

    return Some(pass_out);
}
