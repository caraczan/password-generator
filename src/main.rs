mod password_generator;

use clap::{App, Arg};

// test

fn main() {
    let mut _length_of_password: usize = 16;
    let _help = App::new("Secure Password Generator")
        .version("1.2")
        .author("Caraczan <gitlab.com/caraczan>")
        .about("Generates secure password using secure randomization")
        .arg(
            Arg::with_name("length")
                .short("l")
                .long("length")
                .takes_value(true)
                .help(
                    "Password length, Default 16.\n note: Upon wrong argument (e.g. String or negative numbers) it defaults to 16.",
                ),
        )
        .get_matches();

    if _help.is_present("length") {
        // take from `_help` argument of `length` returns Some<&str> that is unwraped, then parsed to usize,
        // if wrong values is given <usize>16 value is assigned
        _length_of_password = _help.value_of("length").unwrap().parse().unwrap_or(16);
    }

    // takes `usize` number and returns Result(String)
    let _password = password_generator::generate_password(_length_of_password).unwrap();

    // printing to user
    println!("WARNING: Don't show your password to anyone you don't know.\nWARNING: Make sure to remember your password.\nlength of password: {len}\nPASSWORD: {pass}", pass =_password, len = _length_of_password );
}
